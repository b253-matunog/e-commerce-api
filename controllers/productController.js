const Product = require("../models/Product");
const User = require("../models/User");

//=========== Create a new course ===============
module.exports.addProduct = (reqBody, userData) => {

  return User.findById(userData.userId)
    .then(result => {
      if(result.isAdmin === true){

        let newProduct = new Product({
          name : reqBody.name,
          description : reqBody.description,
          price : reqBody.price
        });

        newProduct.save();
        
        return true;
      } else {
        return false;
      }
    }).catch(err => err); 
};

// Retrieve all products
module.exports.getAllProducts = (userData) => {
  return User.findById(userData.userId)
    .then(result => {
      if(result.isAdmin){

        return Product.find({});
      } else {
        return false;
      }
    }).catch(err => err); 
};

// Retrieve all active products
module.exports.getAllActiveProduct = () => {

  return Product.find({ isActive : true })
    .then(result => result)
    .catch(err => err);
};

// Retrieving a specific product
module.exports.getProduct = (reqParams) => {

  return Product.findById(reqParams.productId)
    .then(result => result)
    .catch(err => err);
};

// Update a product
// Information to update a product will be coming from both the URL parameters and the request body
module.exports.updateProduct = (reqParams, reqBody) => {

  // Specify the fields/properties of the document to be updated
  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  }

  return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
    .then(product => true).catch(err => err); 
};

// Archive a product
module.exports.archiveProduct = (reqParams, reqBody) => {

  // Specify the fields/properties of the document to be updated
  let archivedProduct = {
    isActive: reqBody.isActive
  }

  return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
    .then(product => true).catch(err => err); 
};

// Activate a product
module.exports.activateProduct = (reqParams, reqBody) => {

  // Specify the fields/properties of the document to be updated
  let activatedProduct = {
    isActive: reqBody.isActive
  }

  return Product.findByIdAndUpdate(reqParams.productId, activatedProduct)
    .then(product => true).catch(err => err); 
};