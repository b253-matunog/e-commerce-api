const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product
router.post("/", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization)

  productController.addProduct(req.body, { userId : userData.id })
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err))
});

// Route for retrieving all the products
router.get("/all", auth.verify, (req, res) => { // need middleware auth

  const userData = auth.decode(req.headers.authorization)

  productController.getAllProducts({ userId : userData.id })
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
});

// Route for retrieving all the ACTIVE products
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the products
router.get("/", (req, res) => {

  productController.getAllActiveProduct()
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
});

// Route for retrieving a specific product
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:productId", (req, res) => {

  // Since the product ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the product ID by accessing the request's "params" property which contains all the parameters provided via the url
		// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
		// The product Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "productId" in the route
  productController.getProduct(req.params)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
});


// Route for updating a product
// JWT verification is needed for this route to ensure that a user is logged in before updating a course
router.put("/:productId", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization)

  if(userData.isAdmin){
    productController.updateProduct(req.params, req.body)
      .then(resultFromController => res.send(resultFromController))
      .catch(err => res.send(err));
  } else {
    res.send(false);
  }
});

// Route to archiving a product
// A "PUT"/"PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the products from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.patch("/:productId/archive", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization)

  if(userData.isAdmin){
    productController.archiveProduct(req.params, req.body)
      .then(resultFromController => res.send(resultFromController))
      .catch(err => res.send(err));
  } else {
    res.send(false);
  }
});

// Route to activating a product
router.patch("/:productId/activate", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization)

  if(userData.isAdmin){
    productController.activateProduct(req.params, req.body)
      .then(resultFromController => res.send(resultFromController))
      .catch(err => res.send(err));
  } else {
    res.send(false);
  }
});



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;